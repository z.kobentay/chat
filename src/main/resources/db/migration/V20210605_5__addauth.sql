DROP TABLE IF EXISTS auth;
create table auth
(
    login varchar(255),
    password varchar(255),
    last_login_timestamp bigint,
    user_id bigint,
    token varchar(255)
);