package kz.aitu.chat1906.controller;


import kz.aitu.chat1906.service.AuthService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@AllArgsConstructor
@RequestMapping("/api/auth")
public class AuthController {
    private AuthService authService;

    @RequestMapping("/login")
    public ResponseEntity<?> login(@RequestParam(value = "login") String log, @RequestParam(value = "pass") String pass) {
        if (authService.login(log, pass)) {
            return ResponseEntity.ok(log);
        }
        return ResponseEntity.ok("Doesn't exist");
    }
}