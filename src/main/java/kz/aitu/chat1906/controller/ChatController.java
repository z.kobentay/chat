package kz.aitu.chat1906.controller;

<<<<<<< HEAD
import kz.aitu.chat1906.model.Chat;
import kz.aitu.chat1906.repository.ChatRepository;
import kz.aitu.chat1906.service.AuthService;
import kz.aitu.chat1906.service.ChatService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
=======
import kz.aitu.chat1906.repository.ChatRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
>>>>>>> a4571cf3a98bb397704385e7650d15b1f3e7fcca

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/chat")
public class ChatController {
<<<<<<< HEAD
    private final ChatService chatService;
    private final AuthService authService;

    @GetMapping("/find")
    public ResponseEntity<?> getAll(@RequestHeader String token) {
        if (authService.isTokenExist(token)) {
            return ResponseEntity.ok(chatService.findAll());
        }
        return ResponseEntity.ok("Token doesn't exist");
    }

    @PostMapping("/add")
    public ResponseEntity<?> add(@RequestBody Chat chat, @RequestHeader String token) {
        if (authService.isTokenExist(token)) {
            chatService.add(chat);
            return ResponseEntity.ok("Chat added");
        }
        return ResponseEntity.ok("Token doesn't exist");
    }

    @PutMapping("/update")
    public ResponseEntity<?> update(@RequestBody Chat chat, @RequestHeader String token) {
        if (authService.isTokenExist(token)) {
            try {
                chatService.update(chat);
            } catch (Exception e) {
                return ResponseEntity.badRequest().body(e.getMessage());
            }
            return ResponseEntity.ok(chat);
        }
        return ResponseEntity.ok("Token doesn't exist");
    }

    @DeleteMapping("/delete")
    public ResponseEntity<?> delete(@RequestBody Chat chat, @RequestHeader String token) {
        if (authService.isTokenExist(token)) {
            try {
                chatService.delete(chat);
            } catch (Exception e) {
                return ResponseEntity.badRequest().body(e.getMessage());
            }
            return ResponseEntity.ok("Deleted");
        }
        return ResponseEntity.ok("Token doesn't exist");
    }
}
=======
    private ChatRepository ChatRepository;

    @GetMapping({""})
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(ChatRepository.findAll());
    }
}
>>>>>>> a4571cf3a98bb397704385e7650d15b1f3e7fcca
