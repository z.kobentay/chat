package kz.aitu.chat1906.controller;

import kz.aitu.chat1906.model.Message;
<<<<<<< HEAD
import kz.aitu.chat1906.service.AuthService;
import kz.aitu.chat1906.service.MessageService;
import lombok.AllArgsConstructor;
=======
import kz.aitu.chat1906.repository.MessageRepository;
import kz.aitu.chat1906.service.MessageService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
>>>>>>> a4571cf3a98bb397704385e7650d15b1f3e7fcca
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@Controller
@AllArgsConstructor
@RequestMapping("/api/v1/message")
public class MessageController {
    private final MessageService messageService;
<<<<<<< HEAD
    private final AuthService authService;

    @GetMapping("/getall")
    public ResponseEntity<?> getAll(@RequestHeader String token) {
        if(authService.isTokenExist(token)){
            return ResponseEntity.ok(messageService.findAll());
        }
        return ResponseEntity.ok("Token doesn't exist");
    }

    @PostMapping("/add")
    public ResponseEntity<?> add(@RequestBody Message message, @RequestHeader String token) {
        Date date = new Date();
        if(authService.isTokenExist(token)){
            message.setCreatedTimestamp(date.getTime()/1000);
            messageService.addMessage(message);
            return ResponseEntity.ok("Message added");
        }
        return ResponseEntity.ok("Token doesn't exist");

    }

    @PutMapping("/update")
    public ResponseEntity<?> update(@RequestBody Message message, @RequestHeader String token) {
        if(authService.isTokenExist(token)){
            return ResponseEntity.ok(messageService.update(message));
        }

        return ResponseEntity.ok("Token doesn't exist");
    }

    @DeleteMapping("/delete")
    public ResponseEntity<?> delete(@RequestBody Message message, @RequestHeader String token) {
        if(authService.isTokenExist(token)){
            messageService.delete(message);
            return ResponseEntity.ok("Message successfully deleted");
        }
        return ResponseEntity.ok("Token doesn't exist");
    }

    @GetMapping("/getTenMessageByChatId/{id}")
    public ResponseEntity<?> get10MessageByChatId(@PathVariable Long id) {
=======
    private final MessageRepository messageRepository;

    @PostMapping("")
    public ResponseEntity<?> addMessage(@RequestBody Message message){

        return ResponseEntity.status(HttpStatus.CREATED).body(messageService.addMessage(message));
    }

    @PutMapping("")
    public ResponseEntity<?> updateMessage(@RequestBody Message message){
        return ResponseEntity.ok(messageService.updateMessage(message));
    }


    @GetMapping("/getTenMessageByChatId/{id}")
    public ResponseEntity<?> get10MessageByChatId(@PathVariable Long id){
>>>>>>> a4571cf3a98bb397704385e7650d15b1f3e7fcca
        return ResponseEntity.ok(messageService.getLast10MessageByChatId(id));
    }

    @GetMapping("getTenMessageByUserId/{id}")
<<<<<<< HEAD
    public ResponseEntity<?> get10MessageByUserId(@PathVariable Long id) {
        return ResponseEntity.ok(messageService.getLast10MessageByUserId(id));
    }

    @GetMapping({"/chat/{id}"})
    public ResponseEntity<?> getMessagesByChatId(@PathVariable(name = "id") Long chatId) {
        return ResponseEntity.ok(messageService.getAllChatById(chatId));
    }

    @RequestMapping("/isread")
    public ResponseEntity<?> isRead(@RequestBody Message message, @RequestHeader String t){
        if(authService.isTokenExist(t)){
            return ResponseEntity.ok(messageService.isRead(message.getUserId(), message.getId(), message.getRead()));
        }
        return ResponseEntity.ok("Token doesn't exist");
    }

    @RequestMapping("/getnotdelivered/{chatid}")
    public ResponseEntity<?> getAllUndelivered(@PathVariable Long chatid, @RequestHeader String t){
        if(authService.isTokenExist(t)){
            return ResponseEntity.ok(messageService.getAllUndelivered(chatid));
        }
        return ResponseEntity.ok("Token doesn't exist");
    }
=======
    public ResponseEntity<?> get10MessageByUserId(@PathVariable Long id){
        return ResponseEntity.ok(messageService.getLast10MessageByUserId(id));
    }


    @GetMapping({"/chat/{id}"})
    public ResponseEntity<?> getMessagesByChatId(@PathVariable(name = "id") Long chatId){
        return ResponseEntity.ok(messageService.getAllChatById(chatId));
    }

    @DeleteMapping({"/chat/{id}"})
    public ResponseEntity<?> deleteMessage(@PathVariable(name = "id") Long id){
        messageService.deleteMessage(id);
        return ResponseEntity.noContent().build();
    }

>>>>>>> a4571cf3a98bb397704385e7650d15b1f3e7fcca

}
