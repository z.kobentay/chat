package kz.aitu.chat1906.service;

import kz.aitu.chat1906.model.Chat;
import kz.aitu.chat1906.model.Message;
import kz.aitu.chat1906.repository.ChatRepository;
import kz.aitu.chat1906.repository.MessageRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class MessageService {
<<<<<<< HEAD
    private final MessageRepository messageRepository;
    private final ParticipantService participantService;


    public void addMessage(Message message) {
        message.setCreatedTimestamp(new Date().getTime());
        messageRepository.save(message);
    }

    public List<Message> findAll() {
        return messageRepository.findAll();
    }

    public Message update(Message message) {
        Optional<Message> messageDbOptional = messageRepository.findById(message.getId());
        if(messageDbOptional.isPresent()) {
            Message messageDb = messageDbOptional.get();
            messageDb.setText(message.getText());
            messageDb.setUpdatedTimestamp(new Date().getTime());
            return messageRepository.save(messageDb);
        } else {
            return null;
        }
    }

    public void delete(Message message) {
        messageRepository.delete(message);
    }
=======
    private MessageRepository messageRepository;

    public Message addMessage( Message message){
        Date date = new Date();
        message.setCreatedTimestamp(date.getTime());
        messageRepository.save(message);
        return messageRepository.save(message);
    }

    public void deleteMessage(Long id){
        messageRepository.deleteById(id);
    }

    public Message updateMessage(Message message){
        Date date = new Date();
        message.setUpdatedTimestamp(date.getTime());
        messageRepository.save(message);
        return messageRepository.save(message);
    }

>>>>>>> a4571cf3a98bb397704385e7650d15b1f3e7fcca


    public List<Message> getLast10MessageByChatId(Long id){
        return messageRepository.getLast10MessagesByChatId(id);
    }

    public List<Message> getLast10MessageByUserId(Long id){
        return messageRepository.getLast10MessageByUserId(id);
    }



    public List<Message> getAllChatById(Long chatId) {
        List<Message> messageList = this.messageRepository.findAllByChatId(chatId);
        return messageList;
    }
<<<<<<< HEAD

    public Message isRead(Long userId, Long messageId, boolean status) {
        Message message = messageRepository.isRead(userId, messageId);
        message.setRead(status);
        return message;
    }

    public List<Message> getAllUndelivered(Long id) {
        return messageRepository.getAllUndelivered(id);
    }

=======
>>>>>>> a4571cf3a98bb397704385e7650d15b1f3e7fcca
}
