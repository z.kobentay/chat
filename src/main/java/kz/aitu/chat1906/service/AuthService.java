package kz.aitu.chat1906.service;

import kz.aitu.chat1906.model.Auth;
import kz.aitu.chat1906.repository.AuthRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
public class AuthService {
    private AuthRepository authRepository;

    public void add(Auth auth) {
        authRepository.save(auth);
    }

    public List<Auth> getAll() {
        return null;
    }

    public Auth update(Auth auth) {
        return null;
    }

    public void delete(Auth auth) {
    }

    public boolean login(String log, String pass) {
        Auth auth = authRepository.findByLoginAndPassword(log, pass);
        if (auth != null) {
            auth.setLastLoginTimeStamp(new Date().getTime());
            auth.setToken(UUID.randomUUID().toString());
            add(auth);
            return true;
        } else {
            return false;
        }
    }

    public boolean isTokenExist(String token) {
        if (authRepository.isTokenExists(token)) {
            return true;
        }
        return false;
    }
}
