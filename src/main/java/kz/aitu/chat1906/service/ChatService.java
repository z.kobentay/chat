package kz.aitu.chat1906.service;

<<<<<<< HEAD
import kz.aitu.chat1906.model.Chat;
import kz.aitu.chat1906.repository.ChatRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@AllArgsConstructor
public class ChatService {
    private final ChatRepository chatRepository;

    public void add(Chat chat) { chatRepository.save(chat); }

    public List<Chat> findAll() {
        return chatRepository.findAll();
    }

    public Chat update(Chat chat) {
        return chatRepository.save(chat);
    }

    public void delete(Chat chat) {
        chatRepository.delete(chat);
    }
=======
public class ChatService {
>>>>>>> a4571cf3a98bb397704385e7650d15b1f3e7fcca
}
