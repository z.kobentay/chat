package kz.aitu.chat1906.repository;

import kz.aitu.chat1906.model.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
<<<<<<< HEAD
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
=======

import java.util.List;

>>>>>>> a4571cf3a98bb397704385e7650d15b1f3e7fcca
public interface MessageRepository extends JpaRepository <Message, Long> {
    List<Message> findAllByChatId(Long chatId);

    @Query(value = "SELECT * FROM message m WHERE m.chat_id = ?1 ORDER BY m.id DESC LIMIT 10", nativeQuery=true)
    List<Message> getLast10MessagesByChatId(Long chatId);

    @Query(value = "SELECT * FROM message m WHERE m.user_id = ?1 ORDER BY m.id DESC LIMIT 10", nativeQuery=true)
    List<Message> getLast10MessageByUserId(Long userId);

<<<<<<< HEAD
    Message isRead(Long userId, Long messageId);

    List<Message> getAllUndelivered(Long chatid);
=======
>>>>>>> a4571cf3a98bb397704385e7650d15b1f3e7fcca
}
