package kz.aitu.chat1906.repository;

import kz.aitu.chat1906.model.Auth;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthRepository extends JpaRepository<Auth, Long> {

    Auth findByLoginAndPassword(String login, String password);

    boolean isTokenExists(String token);
}
